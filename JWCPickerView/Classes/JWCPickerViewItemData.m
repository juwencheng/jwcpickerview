//
//  JWCPickerViewItemData.m
//  CustomPickerView
//
//  Created by Ju on 4/6/16.
//  Copyright © 2016 OwenJu. All rights reserved.
//

#import "JWCPickerViewItemData.h"

@implementation JWCPickerViewItemData

+ (instancetype)dataWithKey:(NSInteger)key name:(NSString *)name children:(NSArray *)children {
    JWCPickerViewItemData *data = [[self alloc] init];
    data.key = key;
    data.name = name;
    data.children = children;
    return data;
}

+ (instancetype)modelFromJson:(NSDictionary *)json {
    // ready for subclass implements
    return nil;
}

@end
