//
//  JWCPickerViewItemData.h
//  CustomPickerView
//
//  Created by Ju on 4/6/16.
//  Copyright © 2016 OwenJu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JWCPickerViewItemData : NSObject

+ (instancetype)dataWithKey:(NSInteger)key name:(NSString *)name children:(NSArray *)children;

@property(nonatomic, assign) NSInteger key;
@property(nonatomic, strong, nonnull) NSString *name;
@property(nonatomic, strong, nullable) NSArray<JWCPickerViewItemData *> *children;

+ (instancetype)modelFromJson:(NSDictionary *)json;

@end
