//
//  JWCPickerView.h
//  CustomPickerView
//
//  Created by Ju on 4/6/16.
//  Copyright © 2016 OwenJu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JWCPickerViewItemData.h"

@class JWCPickerView;

@interface JWCPickerViewConfig : NSObject

@property(nonatomic, strong, nullable) NSString *configName;
@property(nonatomic, assign) NSInteger numberOfColumns;
@property(nonatomic, strong, nullable) NSArray *columnsPercentages;
@property(nonatomic, strong, nullable) NSArray *columnsHeights;
@property(nonatomic, strong, nullable) id additionObject; // 额外的信息

// 配置信息
@property(nonatomic, strong) UIFont *indicatorLabelFont;
@property(nonatomic, strong) UIColor *indicatorLabelColor;
@end

@protocol JWCPickerViewDelegate <NSObject>

- (void)picker:(JWCPickerView *_Nonnull)picker didClickDoneWithSelection:(NSArray<JWCPickerViewItemData *> *_Nullable)selectedData;

- (void)picker:(JWCPickerView *_Nonnull)picker didClickCancelWithSelection:(NSArray<JWCPickerViewItemData *> *_Nullable)selectedData;

// 重新请求数据
- (void)pickerRetryRequestData:(JWCPickerView *)picker;

@end

typedef enum : NSUInteger {
    JCustomPickerViewStateLoading,
    JCustomPickerViewStateFailed,
    JCustomPickerViewStateSucccess,
    JCustomPickerViewStateNoData,
} JCustomPickerViewState;
// TODO: 增加属性可以自行决定隐藏后是否从父视图删除；增加从父视图删除的方法
@interface JWCPickerView : UIView

- (void)reloadData:(NSArray<JWCPickerViewItemData *> *)data; //重新加载

@property(nonatomic, assign) JCustomPickerViewState state; //状态

@property(nonatomic, assign) NSInteger numberOfColumns;

@property(nonatomic, strong, nonnull) JWCPickerViewConfig *config;

@property(nonatomic, weak, nullable) id <JWCPickerViewDelegate> delegate;

- (void)show;

- (void)hide;

@end
