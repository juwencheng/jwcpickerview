//
//  JWCPickerView.m
//  CustomPickerView
//
//  Created by Ju on 4/6/16.
//  Copyright © 2016 OwenJu. All rights reserved.
//

#import "JWCPickerView.h"
#import "JWCPickerRowView.h"
@implementation JWCPickerViewConfig

- (instancetype)init {
    self = [super init];
    if (self) {

    }
    return self;
}

@end

@interface JWCPickerView () <UIPickerViewDelegate, UIPickerViewDataSource>
@property(nonatomic, strong, nonnull) NSArray *data;
@property(nonatomic, strong) UIPickerView *pickerView;
@property(nonatomic, strong) UIToolbar *toolbar;

@property(nonatomic, strong) UIView *statusView;
@property(nonatomic, strong) UIImageView *indicatorImageView;
@property(nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property(nonatomic, strong) UILabel *indicatorLabel;

@end

@implementation JWCPickerView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    [self addSubview:self.statusView];
    self.statusView.translatesAutoresizingMaskIntoConstraints = NO;

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[statusView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:@{@"statusView": self.statusView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[statusView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:@{@"statusView": self.statusView}]];
    [self sendSubviewToBack:self.statusView];
    [self bindEvents];
}

- (void)bindEvents {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    tapGesture.numberOfTapsRequired = 1;
    [self.statusView addGestureRecognizer:tapGesture];
    self.statusView.userInteractionEnabled = NO;
}

- (void)tapGesture {
    if([self.delegate respondsToSelector:@selector(pickerRetryRequestData:)]) {
        [self.delegate pickerRetryRequestData:self];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (![self.subviews containsObject:self.toolbar]) {
        [self addSubview:self.toolbar];
    }
    if (![self.subviews containsObject:self.pickerView]) {
        [self addSubview:self.pickerView];
    }
    CGRect frame = self.bounds;
    //小余toolbar高度
    CGFloat toolbarHeight = 44;
    if (CGRectGetHeight(frame) <= toolbarHeight) {
        @throw [[NSException alloc] initWithName:@"高度不够" reason:@"自定义pickerView高度至少要大于40" userInfo:nil];
    }
    self.toolbar.frame = (CGRect) {CGPointZero, {CGRectGetWidth(frame), toolbarHeight}};
    frame.size.height -= toolbarHeight;
    frame.origin.y = toolbarHeight;
    self.pickerView.frame = frame;
    [self.pickerView setNeedsLayout];
    [self.pickerView layoutIfNeeded];
    [self.pickerView reloadAllComponents];
}

- (void)reloadData:(NSArray *)data {
    _data = data;
    [self.pickerView setNeedsLayout];
    [self.pickerView layoutIfNeeded];
}

- (void)setState:(JCustomPickerViewState)state {
    _state = state;
    if (_state == JCustomPickerViewStateFailed) {
        [self bringSubviewToFront:self.statusView];
        self.statusView.userInteractionEnabled = YES;
        self.indicatorLabel.text = @"数据加载失败，点击重试";
        self.indicatorLabel.textColor = [UIColor redColor];
        self.indicatorImageView.image = [[UIImage imageNamed:@"icon-close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.indicatorImageView.tintColor = [UIColor redColor];
        [self.statusView sendSubviewToBack:self.activityIndicator];
        [self.activityIndicator stopAnimating];
    } else if (_state == JCustomPickerViewStateLoading) {
        [self bringSubviewToFront:self.statusView];
        self.statusView.userInteractionEnabled = NO;
        self.indicatorLabel.textColor = [UIColor blackColor];
        [self.statusView bringSubviewToFront:self.activityIndicator];
        [self.activityIndicator startAnimating];
        self.indicatorLabel.text = @"数据加载中...";
    } else if (_state == JCustomPickerViewStateSucccess) {
        [self sendSubviewToBack:self.statusView];
        self.statusView.userInteractionEnabled = NO;
        [self.activityIndicator stopAnimating];
        self.indicatorLabel.textColor = [UIColor blackColor];
    } else if (_state == JCustomPickerViewStateNoData) {
        [self bringSubviewToFront:self.statusView];
        self.statusView.userInteractionEnabled = YES;
        self.indicatorLabel.textColor = [UIColor redColor];
        self.indicatorLabel.text = @"获取数据为空，点击重试~~";
        self.indicatorImageView.tintColor = [UIColor redColor];
        self.indicatorImageView.image = [[UIImage imageNamed:@"voice_to_short"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.statusView sendSubviewToBack:self.activityIndicator];
        [self.activityIndicator stopAnimating];
    }
}

- (void)setConfig:(JWCPickerViewConfig *)config {
    _config = config;
    if (config.indicatorLabelFont) {
        self.indicatorLabel.font = config.indicatorLabelFont;
    }
    if (config.indicatorLabelColor) {
        self.indicatorLabel.textColor = config.indicatorLabelColor;
    }
}

- (void)show {
    //判断目前是否显示
    [self.superview endEditing:YES];
    if (self.frame.origin.y >= CGRectGetHeight(self.superview.frame)) {
        [UIView animateWithDuration:0.25 delay:0.1 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            CGRect frame = self.frame;
            frame.origin.y = (CGFloat) (CGRectGetHeight(self.superview.frame) - frame.size.height);
            self.frame = frame;
        }                completion:nil];
    }
}

- (void)hide {
    if (self.frame.origin.y < CGRectGetHeight(self.superview.frame)) {
        //do hide
        [UIView animateWithDuration:0.25 delay:0.1 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.frame = CGRectOffset(self.frame, 0, CGRectGetHeight(self.frame));
        }                completion:nil];
    }
}

#pragma mark - picker view delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    if (self.config.columnsHeights && self.config.columnsHeights.count > component) {
        return (CGFloat) [self.config.columnsHeights[(NSUInteger) component] doubleValue];
    }
    return 40.0f;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (self.config.columnsPercentages) {
        float total = 0;
        for (NSNumber *number in self.config.columnsPercentages) {
            total += [number floatValue];
        }
        float scale = (float) (1.0 / total);
        if (self.config.columnsPercentages.count == self.config.numberOfColumns) {
            return [self.config.columnsPercentages[(NSUInteger) component] floatValue] * scale * CGRectGetWidth(pickerView.frame);
        } else {
            NSLog(@"宽度比例个数不一致，请检查，默认均分");
            return CGRectGetWidth(self.pickerView.frame) / self.config.numberOfColumns;
        }
    }
    return CGRectGetWidth(self.pickerView.frame) / self.config.numberOfColumns;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.config.numberOfColumns;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    //retrive previous selected model and get it's children
    if (self.data && self.data.count > 0) {
        if (component == 0) {
            return self.data.count;
        } else {
            NSInteger selected = 0;
            int i = 0;
            NSArray *data = self.data;
            while (i != component) {
                selected = [pickerView selectedRowInComponent:i];
                JWCPickerViewItemData *model = data[MIN(data.count - 1, selected)];
                if (model.children.count == 0)
                    break;

                data = model.children;
                i++;
            }
            return data.count;
        }
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    for (NSInteger i = component + 1; i < self.config.numberOfColumns; i++) {
        [self.pickerView reloadComponent:i];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    JWCPickerRowView *rowView = (JWCPickerRowView *)view;
    if (!rowView) {
        rowView = [[JWCPickerRowView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 0)];
    }
    NSInteger selected = 0;
    int i = 0;
    NSArray *data = self.data;
    while (i != component) {
        selected = [pickerView selectedRowInComponent:i];
        selected = MAX(selected, -1);
        JWCPickerViewItemData *model = data[MIN(selected, data.count - 1)];
        if (model.children.count == 0)
            return nil;

        data = model.children;
        i++;
    }
    JWCPickerViewItemData *result = data[MIN(row, data.count - 1)];
    rowView.data = result;
    return rowView;
}

#pragma mark - 事件
- (void)clickDone:(UIBarButtonItem *)done {
    if ([self.delegate respondsToSelector:@selector(picker:didClickDoneWithSelection:)]) {
        NSMutableArray *selection = [NSMutableArray array];
        NSArray *data = self.data;
        for (NSInteger i = 0; i < self.config.numberOfColumns; i++) {
            NSInteger selected = [self.pickerView selectedRowInComponent:i];
            selected = MAX(selected, 0);
            selected = MIN(data.count - 1, selected);
            if (data.count > selected) {
                [selection addObject:data[(NSUInteger) selected]];
                data = [data[(NSUInteger) selected] children];
            }
        }
        [self.delegate picker:self didClickDoneWithSelection:selection];
    }
    if (self.superview) {
        [self hide];
    }
}

- (void)clickCancel:(UIBarButtonItem *)cancel {
    if ([self.delegate respondsToSelector:@selector(picker:didClickCancelWithSelection:)]) {
        [self.delegate picker:self didClickCancelWithSelection:nil];
    }
    if (self.superview) {
        [self hide];
    }
}

#pragma mark - 属性
- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        _pickerView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    }
    return _pickerView;
}

- (UIToolbar *)toolbar {
    if (!_toolbar) {
        _toolbar = [[UIToolbar alloc] init];
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(clickCancel:)];
        UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(clickDone:)];
        _toolbar.items = @[cancel, flexible, done];
    }
    return _toolbar;
}

- (UIImageView *)indicatorImageView {
    if (!_indicatorImageView) {
        _indicatorImageView = [[UIImageView alloc] init];
        _indicatorImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _indicatorImageView;
}

- (UIActivityIndicatorView *)activityIndicator {
    if (!_activityIndicator) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    }
    return _activityIndicator;
}

- (UILabel *)indicatorLabel {
    if (!_indicatorLabel) {
        _indicatorLabel = [[UILabel alloc] init];
        _indicatorLabel.font = [UIFont systemFontOfSize:16];
        _indicatorLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _indicatorLabel;
}

- (UIView *)statusView {
    if (!_statusView) {
        _statusView = [[UIView alloc] init];

        [_statusView addSubview:self.indicatorLabel];
        [_statusView addSubview:self.indicatorImageView];
        [_statusView addSubview:self.activityIndicator];

        self.indicatorLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.indicatorImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *dict = @{@"label": self.indicatorLabel, @"imageView": self.indicatorImageView, @"activity": self.activityIndicator};
        NSDictionary *metric = @{@"imageWidth": @(80)};
        [_statusView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[imageView(==imageWidth)]" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metric views:dict]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.indicatorImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_statusView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.indicatorImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_statusView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [_statusView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[label]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metric views:dict]];
        [_statusView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[imageView]-16-[label]" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metric views:dict]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
    }
    return _statusView;
}
@end
