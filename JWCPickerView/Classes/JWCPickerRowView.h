//
// Created by 鞠汶成 on 2018/9/20.
//

#import <Foundation/Foundation.h>

@class JWCPickerViewItemData;

@interface JWCPickerRowView : UIView

@property(nonatomic, weak) JWCPickerViewItemData *data;

@property(nonatomic, strong) UILabel *label;

@end