//
//  JWCFlatPickerView.h
//  FBSnapshotTestCase
//
//  Created by 鞠汶成 on 2018/11/8.
//

#import <Foundation/Foundation.h>
#import "JWCPickerView.h"
NS_ASSUME_NONNULL_BEGIN

@interface JWCFlatPickerView : UIView

// 多维数组
- (void)reloadData:(NSArray *)data; //重新加载

@property(nonatomic, assign) JCustomPickerViewState state; //状态

@property(nonatomic, strong, nonnull) JWCPickerViewConfig *config;

@property(nonatomic, weak, nullable) id <JWCPickerViewDelegate> delegate;

- (void)show;

- (void)hide;
@end

NS_ASSUME_NONNULL_END
