//
//  JWCFlatPickerView.m
//  FBSnapshotTestCase
//
//  Created by 鞠汶成 on 2018/11/8.
//

#import "JWCFlatPickerView.h"
#import "JWCPickerRowView.h"
@interface JWCFlatPickerView()
@property(nonatomic, strong, nonnull) NSArray<NSArray*> *data;
@property(nonatomic, strong) NSMutableArray *componentsWidth;
@property(nonatomic, strong) UIPickerView *pickerView;
@property(nonatomic, strong) UIToolbar *toolbar;

@property(nonatomic, strong) UIView *statusView;
@property(nonatomic, strong) UIImageView *indicatorImageView;
@property(nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property(nonatomic, strong) UILabel *indicatorLabel;
@end

@implementation JWCFlatPickerView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    [self addSubview:self.statusView];
    self.statusView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[statusView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:@{@"statusView": self.statusView}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[statusView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:@{@"statusView": self.statusView}]];
    [self sendSubviewToBack:self.statusView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (![self.subviews containsObject:self.toolbar]) {
        [self addSubview:self.toolbar];
    }
    if (![self.subviews containsObject:self.pickerView]) {
        [self addSubview:self.pickerView];
    }
    CGRect frame = self.bounds;
        //小余toolbar高度
    CGFloat toolbarHeight = 44;
    if (CGRectGetHeight(frame) <= toolbarHeight) {
        @throw [[NSException alloc] initWithName:@"高度不够" reason:@"自定义pickerView高度至少要大于40" userInfo:nil];
    }
    self.toolbar.frame = (CGRect) {CGPointZero, {CGRectGetWidth(frame), toolbarHeight}};
    frame.size.height -= toolbarHeight;
    frame.origin.y = toolbarHeight;
    self.pickerView.frame = frame;
    [self.pickerView setNeedsLayout];
    [self.pickerView layoutIfNeeded];
    [self.pickerView reloadAllComponents];
}

- (void)reloadData:(NSArray *)data {
    _data = data;
    if (data.count == 0) return;
    // 设置宽度
    [self.componentsWidth removeAllObjects];
    if (self.config.columnsPercentages && self.config.columnsPercentages.count == data.count) {
        CGFloat staticWidth = 0;
        CGFloat flexTotal = 0;
        for (NSNumber *number in self.config.columnsPercentages) {
            if ([number doubleValue] > 1) {
                staticWidth += [number doubleValue];
            }else {
                flexTotal += [number doubleValue];
            }
        }
        CGFloat restWidth = self.pickerView.frame.size.width - staticWidth;
        for (NSNumber *number in self.config.columnsPercentages) {
            if ([number doubleValue] > 1) {
                [self.componentsWidth addObject:number];
            }else {
                [self.componentsWidth addObject:@([number doubleValue] * 1 / flexTotal * restWidth)];
            }
        }
    }else {
        NSInteger count = data.count;
        CGFloat componentWidth = self.pickerView.frame.size.width/data.count;
        while (count) {
            [self.componentsWidth addObject:@(componentWidth)];
            count--;
        }
    }
    [self.pickerView reloadAllComponents];
//    [self.pickerView setNeedsLayout];
//    [self.pickerView layoutIfNeeded];
}

- (void)setState:(JCustomPickerViewState)state {
    _state = state;
    if (_state == JCustomPickerViewStateFailed) {
        [self bringSubviewToFront:self.statusView];
        self.indicatorLabel.text = @"数据加载失败";
        self.indicatorLabel.textColor = [UIColor redColor];
        self.indicatorImageView.image = [[UIImage imageNamed:@"icon-close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.indicatorImageView.tintColor = [UIColor redColor];
        [self.statusView sendSubviewToBack:self.activityIndicator];
        [self.activityIndicator stopAnimating];
        
    } else if (_state == JCustomPickerViewStateLoading) {
        [self bringSubviewToFront:self.statusView];
        self.indicatorLabel.textColor = [UIColor blackColor];
        [self.statusView bringSubviewToFront:self.activityIndicator];
        [self.activityIndicator startAnimating];
        self.indicatorLabel.text = @"数据加载中...";
    } else if (_state == JCustomPickerViewStateSucccess) {
        [self sendSubviewToBack:self.statusView];
        [self.activityIndicator stopAnimating];
        self.indicatorLabel.textColor = [UIColor blackColor];
    } else if (_state == JCustomPickerViewStateNoData) {
        [self bringSubviewToFront:self.statusView];
        self.indicatorLabel.textColor = [UIColor redColor];
        self.indicatorLabel.text = @"没有找到数据，请稍后再试~~";
        self.indicatorImageView.tintColor = [UIColor redColor];
        self.indicatorImageView.image = [[UIImage imageNamed:@"voice_to_short"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.statusView sendSubviewToBack:self.activityIndicator];
        [self.activityIndicator stopAnimating];
    }
}

- (void)show {
        //判断目前是否显示
    [self.superview endEditing:YES];
    if (self.frame.origin.y >= CGRectGetHeight(self.superview.frame)) {
        [UIView animateWithDuration:0.25 delay:0.1 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            CGRect frame = self.frame;
            frame.origin.y = (CGFloat) (CGRectGetHeight(self.superview.frame) - frame.size.height);
            self.frame = frame;
        }                completion:nil];
    }
}

- (void)hide {
    if (self.frame.origin.y < CGRectGetHeight(self.superview.frame)) {
            //do hide
        [UIView animateWithDuration:0.25 delay:0.1 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.frame = CGRectOffset(self.frame, 0, CGRectGetHeight(self.frame));
        }                completion:nil];
    }
}

#pragma mark - picker view delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    if (self.config.columnsHeights && self.config.columnsHeights.count > component) {
        return (CGFloat) [self.config.columnsHeights[(NSUInteger) component] doubleValue];
    }
    return 40.0f;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (self.componentsWidth.count > 0) {
        CGFloat width = [self.componentsWidth[component] doubleValue];
        return width;
    }
    return self.pickerView.frame.size.width;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return self.data.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
        //retrive previous selected model and get it's children
    if (self.data && self.data.count > 0) {
        NSArray *componentData = self.data[component];
        return componentData.count;
    }
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    if (self.data.count== 0) return nil;
    JWCPickerRowView *rowView = (JWCPickerRowView *)view;
    if (!rowView) {
        rowView = [[JWCPickerRowView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 0)];
    }
    NSArray *data = self.data[component];
    JWCPickerViewItemData *result = data[row];
    rowView.data = result;
    return rowView;
}

#pragma mark - 事件
- (void)clickDone:(UIBarButtonItem *)done {
    if ([self.delegate respondsToSelector:@selector(picker:didClickDoneWithSelection:)]) {
        NSMutableArray *selection = [NSMutableArray array];
        for (NSInteger i = 0; i < self.data.count; i++) {
            NSInteger selected = [self.pickerView selectedRowInComponent:i];
            selected = MAX(selected, 0);
            selected = MIN(self.data[i].count - 1, selected);
            [selection addObject:self.data[i][(NSUInteger) selected]];
        }
        [self.delegate picker:self didClickDoneWithSelection:selection];
    }
    if (self.superview) {
        [self hide];
    }
}

- (void)clickCancel:(UIBarButtonItem *)cancel {
    if ([self.delegate respondsToSelector:@selector(picker:didClickCancelWithSelection:)]) {
        [self.delegate picker:self didClickCancelWithSelection:nil];
    }
    if (self.superview) {
        [self hide];
    }
}

#pragma mark - 属性
- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        _pickerView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    }
    return _pickerView;
}

- (UIToolbar *)toolbar {
    if (!_toolbar) {
        _toolbar = [[UIToolbar alloc] init];
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(clickCancel:)];
        UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(clickDone:)];
        _toolbar.items = @[cancel, flexible, done];
    }
    return _toolbar;
}

- (NSMutableArray *)componentsWidth {
    if (!_componentsWidth) {
        _componentsWidth = [NSMutableArray array];
    }
    return _componentsWidth;
}

- (UIImageView *)indicatorImageView {
    if (!_indicatorImageView) {
        _indicatorImageView = [[UIImageView alloc] init];
        _indicatorImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _indicatorImageView;
}

- (UIActivityIndicatorView *)activityIndicator {
    if (!_activityIndicator) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    }
    return _activityIndicator;
}

- (UILabel *)indicatorLabel {
    if (!_indicatorLabel) {
        _indicatorLabel = [[UILabel alloc] init];
        _indicatorLabel.font = [UIFont systemFontOfSize:16];
        _indicatorLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _indicatorLabel;
}

- (UIView *)statusView {
    if (!_statusView) {
        _statusView = [[UIView alloc] init];
        
        [_statusView addSubview:self.indicatorLabel];
        [_statusView addSubview:self.indicatorImageView];
        [_statusView addSubview:self.activityIndicator];
        
        self.indicatorLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.indicatorImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *dict = @{@"label": self.indicatorLabel, @"imageView": self.indicatorImageView, @"activity": self.activityIndicator};
        NSDictionary *metric = @{@"imageWidth": @(80)};
        [_statusView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[imageView(==imageWidth)]" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metric views:dict]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.indicatorImageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_statusView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.indicatorImageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_statusView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
        [_statusView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[label]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metric views:dict]];
        [_statusView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[imageView]-16-[label]" options:NSLayoutFormatDirectionLeadingToTrailing metrics:metric views:dict]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        [_statusView addConstraint:[NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.indicatorImageView attribute:NSLayoutAttributeHeight multiplier:1 constant:0]];
    }
    return _statusView;
}
@end
