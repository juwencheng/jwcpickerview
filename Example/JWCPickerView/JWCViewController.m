//
//  JWCViewController.m
//  JWCPickerView
//
//  Created by Owen Ju on 09/20/2018.
//  Copyright (c) 2018 Owen Ju. All rights reserved.
//

#import "JWCPickerView.h"
#import "JWCFlatPickerView.h"
#import "JWCViewController.h"

@interface JWCViewController ()<JWCPickerViewDelegate>
@property (nonatomic, strong) JWCPickerView *pickerView;
@property (nonatomic, strong) JWCPickerViewConfig *pickerConfig;
@end

@implementation JWCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupPickerView];
    

}
- (IBAction)showPicker:(id)sender {
    self.pickerView.state = JCustomPickerViewStateFailed;
    [self.pickerView show];
//    [self.pickerView reloadData:@[@[[JWCPickerViewItemData dataWithKey:1 name:@"1" children:nil]],@[[JWCPickerViewItemData dataWithKey:1 name:@"2" children:nil]],@[[JWCPickerViewItemData dataWithKey:1 name:@"3" children:nil]]]];
}


- (void)setupPickerView {
    CGRect frame = [UIScreen mainScreen].bounds;
    frame.origin.y = frame.size.height;
    frame.size.height /= 3.0f;
    self.pickerView.frame = frame;
    self.pickerView.config = self.pickerConfig;
    [self.view addSubview:self.pickerView];
    [self.view bringSubviewToFront:self.pickerView];
}

- (JWCPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[JWCPickerView alloc] init];
        _pickerView.delegate = self;
        _pickerView.config = self.pickerConfig;
    }
    return _pickerView;
}

- (JWCPickerViewConfig *)pickerConfig {
    if (!_pickerConfig) {
        _pickerConfig = [[JWCPickerViewConfig alloc] init];
        _pickerConfig.columnsPercentages = @[ @40, @(0.1),@(0.1)];
    }
    return _pickerConfig;
}

- (void)picker:(JWCPickerView * _Nonnull)picker didClickCancelWithSelection:(NSArray<JWCPickerViewItemData *> * _Nullable)selectedData {
    
}

- (void)picker:(JWCPickerView * _Nonnull)picker didClickDoneWithSelection:(NSArray<JWCPickerViewItemData *> * _Nullable)selectedData {
    
}

- (void)pickerRetryRequestData:(JWCPickerView *)picker {
    NSLog(@"retry");
}

@end
