//
//  JWCAppDelegate.h
//  JWCPickerView
//
//  Created by Owen Ju on 09/20/2018.
//  Copyright (c) 2018 Owen Ju. All rights reserved.
//

@import UIKit;

@interface JWCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
