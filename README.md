# JWCPickerView

[![CI Status](https://img.shields.io/travis/Owen Ju/JWCPickerView.svg?style=flat)](https://travis-ci.org/Owen Ju/JWCPickerView)
[![Version](https://img.shields.io/cocoapods/v/JWCPickerView.svg?style=flat)](https://cocoapods.org/pods/JWCPickerView)
[![License](https://img.shields.io/cocoapods/l/JWCPickerView.svg?style=flat)](https://cocoapods.org/pods/JWCPickerView)
[![Platform](https://img.shields.io/cocoapods/p/JWCPickerView.svg?style=flat)](https://cocoapods.org/pods/JWCPickerView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JWCPickerView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JWCPickerView'
```

## Author

Owen Ju, owenju@users.noreply.github.com

## License

JWCPickerView is available under the MIT license. See the LICENSE file for more info.
